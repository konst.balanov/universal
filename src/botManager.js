var moment = require('moment');
var schedlule = [];
var timeouts = {};

// var chrome = {
// 	runtime: {
// 		sendMessage: function (message) {
// 			console.log(message)
// 		}
// 	}
// };

function calculateDifference(time) {
    if (time && time != "Invalid date") {
        let now = new Date() / 1000;
        return moment(time).unix() - now
    } else {
        return false
    }
}

// По таймеру отправлять сигналы на планированое

function sendSignal(tabid, plan) {
    let opendelay = Math.random()*18000 + 25000
    setTimeout(function () {
        chrome.tabs.sendMessage(tabid, {type: 'startScheduledAction', plantype: plan.type})
    }, opendelay)
}

let testPlan = {
    "_id": "5d12183626be2a54c18d3cfb",
    "botId": "8823jf84",
    "classNames": [
        "red"
    ],
    "end": "2019-06-25T20:39:00",
    "id": "_lba00oyta",
    "start": "2019-06-25T20:45:00",
    "title": "Теннис Луз",
    "type": 0
}

function sendStartSignal(argument) {
    // body...
}

function processPlan(plan) {
    let diffStart = calculateDifference(plan.start);
    let diffEnd = calculateDifference(plan.end);

    if (!diffEnd) {
        diffEnd = diffStart + 420;
    }

    if (diffStart < 0 && diffEnd < 0) return false;
    if (diffStart < 0 && diffEnd > 60) {
    	diffStart = 1
    }

    console.log('Поставил таймер для старта', plan.title, plan.id, 'начнётся через', diffStart)
    let startTimeoutId = setTimeout(function() {
        console.log('Наступило время', plan.title, plan.id)

        chrome.tabs.query({}, function (tabs) {
            let bettab = tabs.filter(tb => {if (tb.url.indexOf('/live/') != -1) return true} )


            if (bettab.length) {
                console.log('Отправил сигнал на вкладку', bettab[0].title, 'plan', plan)
                sendSignal(bettab[0].id, plan)
            } 
            else {
                let back = chrome.runtime.getBackgroundPage(function (back) {
                    console.log('Отправил сигнал в новую вкладку plan', plan)
                    back.openLivePage3(sendSignal, plan)
                })
            }
        })
        
    }, diffStart*1000)

    console.log('Поставил таймер для отключения', plan.title, plan.id, 'начнётся через', diffEnd)
    let endTimeoutId = setTimeout(function() {
        console.log('Наступило время вырубать', plan.title, plan.id)
        chrome.runtime.sendMessage({type: 'startScheduledAction', plan})
        delete timeouts[plan.id]
        // chrome.runtime.sendMessage("STOP" + plan.id)
    }, diffEnd*1000)

    timeouts[plan.id] = {
        startTimeoutId,
        endTimeoutId
    }
    console.log(timeouts)
}

// processPlan(testPlan)


module.exports = function(schedluleQ) {
    schedlule = schedluleQ;
    for (k in timeouts) { 
    	clearTimeout(timeouts[k].startTimeoutId);
        clearTimeout(timeouts[k].endTimeoutId); 
        delete timeouts[k]
    }

    // timeouts.forEach(out => )
    schedlule.forEach(dule => processPlan(dule))
}