let io = require('socket.io-client');
var socket = io('http://93.76.249.123:3000');
var axios = require('axios')
let {options, updateOptions} = require('./../options.js');

let cache = {};


socket.on('goodornot', function(data) {
    chrome.tabs.query({}, function(tabs) {
        let tb = tabs.filter(t => t.url.indexOf('/live/') != -1)
        chrome.tabs.sendMessage(tb[0].id, { type: 'allowstart', allow: data }, function(response) {});
    }.bind(this));
});

socket.on('updateschedule', function(data) {
    setTimeout(updateschedule.bind(this, data), 2000)
})

function checkMe() {
    socket.emit('wearegood', { ii: 'iiited' })
}

function splitScores(score) {
    let obj = {

    };

    function extractScores(sc) {
        let obj = {};
        let g = sc.split(':');
        obj.h = g[0];
        obj.g = g[1];
        return obj
    }
    console.log(score)
    let s = score.split(' (');
    s[1] = s[1].split(')')[0];
    let se = s[1].split(',');
    let ress = se.map(function(r) {
        return extractScores(r);
    })
    obj.h = s[0].split(':')[0];
    obj.g = s[0].split(':')[1];

    obj.r = ress
    return obj;
}

function transfromscores(score) {
    return score.replace(/(\![0-9][0-9]|\![0-9])/g, function(match, p1, p2) { console.log(match, p1); return `([${p1.replace('!', '')}-9]|[0-9][0-9])` });
}

function getMatchHistory(sportid, team1, team2, scores, callback) {
    team1 = team1.trim().replace(/\(/g, '\\(').replace(/\)/g, '\)')
    team2 = team2.trim().replace(/\(/g, '\\(').replace(/\)/g, '\)')

    if (sportid == 215) {
        scores = scores.replace(/(\![0-9][0-9]|\![0-9])/g, '').replace(/,:/g, '')
    } else {
        scores = transfromscores(scores)
    }


    if (cache[`${sportid}:${team1}:${team2}:${scores}`]) {
        callback(cache[`${sportid}:${team1}:${team2}:${scores}`])
        return;
    }
    console.log(options)
    fetch(`http://93.76.249.123:3000/e6ij9ik0?sportid=${sportid}&opp1=${team1}&opp2=${team2}&score=${scores}&sendtobot=${options.sendToBot}`).
    then(data => {
        console.log(data)

        if (data.json) {
            return data.json()
        } else {
            callback({})
        }
    }).
    then(data => {
        console.log(data);
        if (data.stutus) {
            cache[`${sportid}:${team1}:${team2}:${scores}`] = {
                totals: data.winrates.calclessmore,
                probablyscores: data.winrates.probablyscores2,
                
                w1: data.winrates.finishWinrate1,
                w2: data.winrates.finishWinrate2,
                w3: data.winrates.finishWinrate3,

                ww1: data.winrates.win1BeforeOt,
                ww2: data.winrates.win2BeforeOt,
                ww0: data.winrates.win3BeforeOt,

                OTY: data.winrates.overtimeYes,
                OTN: data.winrates.overtimeNo,
            }
            callback({
                totals: data.winrates.calclessmore,
                probablyscores: data.winrates.probablyscores2,
                
                w1: data.winrates.finishWinrate1,
                w2: data.winrates.finishWinrate2,
                w3: data.winrates.finishWinrate3,

                ww1: data.winrates.win1BeforeOt,
                ww2: data.winrates.win2BeforeOt,
                ww0: data.winrates.win3BeforeOt,

                OTY: data.winrates.overtimeYes,
                OTN: data.winrates.overtimeNo,
            })
        } else {
            callback({})
        }
    })
}

chrome.runtime.onMessage.addListener(function(request, callback, sendResponse) {
    if (request.type == 'updateMatches') {
        if (request.matches.length) {
            clearMatchesFromStorage();
            // saveMatchesForExt(request.matches);
        } else {
            clearMatchesFromStorage();
        }
    } else if (request.type == 'updateOptions') {



    } else if (request.type == 'gethistory') {
        // console.log(request.sportid, request.team1, request.team2, request.scorequery)

        getMatchHistory(request.sportid, request.team1, request.team2, request.scorequery, sendResponse)
        return true;

    } else if (request.type == 'checkifallow') {
        console.log('checkifallow')
        checkMe()
    }
})

chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { hostEquals: 'ua1xbet.com' },
        })],
        actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
});
// });

function clearMatchesFromStorage() {
    chrome.storage.local.remove('matches', function(res) {

    })
}

function transformTime(time) {

}

function saveMatchesForExt(matches) {
    matches = matches.map(function(m) {
        return { matchName: m.matchName, scores: m.scores }
    })
    chrome.storage.local.set({ matches }, function(response) {
        chrome.runtime.sendMessage({ type: 'readyToExtract' }, function(respond) {

        })
    });
}

function saveToStorage(name, value, cb) {
    let obj = {};
    obj[name] = value

    chrome.storage.local.set(obj, function(respo) {
        if (cb)
            cb()
    })
}

function getFromStorage(name, cb) {
    chrome.storage.local.get(name, function(result) {
        if (cb) {
            cb(result);
        }
    })
}