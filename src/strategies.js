let {options, updateOptions} = require('./options.js')

let strategies = [{
        // Первый выиграет всю игру
        name: 'W1',
        // 0 Общая игра, 1 Второй сет
        sectionType: 0,
        id: 1,
        timeoutId: 0,
        buttonDataType: 1,
        buttons: [],
        sectionElement: null,
        processButton(analysis, btn, id) {
            let coef = this.extractCoef(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];

            // Сеты начинаются с 1
            let currset = lead[0] + lead[1] + 1;

            if (currset == 2 && lead[0] < lead[1]) {
                return false
            }

            if (analysis.w1) { 
                minimumCoef = 1 / (analysis.w1 / 100) + grow 
            }

            return {
                goodIdea: coef > minimumCoef,
                minimumCoef,
                coef,
                btn,
                chance: analysis.w1
            }
        },
        rule() {
            let expData = this.vi.$store.getters.expectedWinnerForCurrentSet;
            return expData
        }
    },
    {
        // Второй выиграет всю игру
        name: 'W2',
        // 0 Общая игра, 1 Второй сет
        sectionType: 0,
        id: 2,
        timeoutId: 0,
        buttonDataType: 3,
        buttons: [],
        sectionElement: null,
        processButton(analysis, btn, id) {
            let coef = this.extractCoef(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];

            let currset = lead[0] + lead[1] + 1;

            if (currset == 2 && lead[0] > lead[1]) {
                return false
            }

            if (analysis.w2) {
                minimumCoef = 1 / (analysis.w2 / 100) + grow
            }
            return {
                goodIdea: coef > minimumCoef,
                minimumCoef,
                coef,
                btn,
                chance: analysis.w2
            }
        },
        rule() {
            let expData = this.vi.$store.getters.expectedWinnerForCurrentSet;
            return expData
        }
    },
    {
        // Первый выиграет второй сет
        name: 'S2W1',
        // 0 Общая игра, 1 Второй сет
        sectionType: 1,
        id: 3,
        timeoutId: 0,
        buttonDataType: 1,
        buttons: [],
        sectionElement: null,
        processButton(analysis, btn, id) {
            let coef = this.extractCoef(btn);
            let grow = options.growCoef;

            if (this.vi.$store.getters.currentSet != 1) return false;

            let minimumCoef = 1000;

            if (analysis.w1) { minimumCoef = 1 / (analysis.w1 / 100) + grow }

            return {
                goodIdea: coef > minimumCoef,
                minimumCoef,
                coef,
                btn,
                chance: analysis.w1
            }
        },
        rule() {
            let expData = this.vi.$store.getters.expectedWinnerForCurrentSet;
            return expData
        }
    },
    {
        name: 'S2W2',
        // 0 Общая игра, 1 Второй сет
        sectionType: 1,
        id: 4,
        timeoutId: 0,
        buttonDataType: 3,
        buttons: [],
        sectionElement: null,
        processButton(analysis, btn, id) {
            let coef = this.extractCoef(btn);
            let grow = options.growCoef;
            if (this.vi.$store.getters.currentSet != 1) return false;

            let minimumCoef = 1000;

            if (analysis.w2) minimumCoef = 1 / (analysis.w2 / 100) + grow

            return {
                goodIdea: coef > minimumCoef,
                minimumCoef,
                coef,
                btn,
                chance: analysis.w2
            }
        },
        rule() {
            let expData = this.vi.$store.getters.expectedWinnerForCurrentSet;
            return expData
        }
    },
    {
        // Матч тотал Больше
        name: 'TB',
        // 0 Общая игра, 1 Второй сет
        sectionType: 0,
        id: 5,
        timeoutId: 0,
        buttonDataType: 9,
        buttons: [],
        sectionElement: null,
        ordering (a, b) {
            return a.total > b.total;
        },
        processButton(analysis, btn, id) {

            // console.log('Кнопочка', btn)
            let coef = this.extractCoef(btn);

            let promTotal = this.extractTotal(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];
            let t1 = this.scores[2] ? this.scores[1].reduce((p, c) => p + Number(c), 0) : 0;
            let t2 = this.scores[3] ? this.scores[2].reduce((p, c) => p + Number(c), 0) : 0;
            let nowWinning = lead[0] > lead[1] ? 1 : (lead[0] < lead[1] ? 2 : 0);
            // Сеты начинаются с 1
            let currset = lead[0] + lead[1] + 1;

            let w1Chance = Math.round((analysis.wins.p1 / analysis.g) * 100);
            let w2Chance = 100 - w1Chance;

            let tfor1 = [];
            let tfor2 = [];

            for (t in analysis.ttls) {
                // console.log('Analiz', analysis.ttls)
                if (analysis.ttls[t].p1 > analysis.ttls[t].p2 && promTotal < (analysis.ttls[t].total + t1 + t2)) {
                    tfor1.push({
                        t: analysis.ttls[t].total + t1 + t2,
                        c: analysis.ttls[t].count,
                    })
                } else if (analysis.ttls[t].p1 < analysis.ttls[t].p2 && promTotal < (analysis.ttls[t].total + t1 + t2)) {
                    tfor2.push({
                        t: analysis.ttls[t].total + t1 + t2,
                        c: analysis.ttls[t].count,
                    })
                }
            }


            tfor1.sort((a, b) => b.c - a.c)
            tfor2.sort((a, b) => b.c - a.c)
            let count = 0;
            if (nowWinning == 1 && tfor1.length) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                minimumCoef = 1 / (count / analysis.g) + grow;


            } else if (nowWinning == 2 && tfor2.length) {
                count = tfor2.reduce((p, c) => { return p + c.c; }, 0)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 0) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                count += tfor2.reduce((p, c) => { return p + c.c; }, count)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else {
                // console.log("Сасайка")
                return false
            }

            return {
                goodIdea: coef > minimumCoef,
                total: promTotal,
                minimumCoef,
                coef,
                btn,
                chance: (count / analysis.g) * 100
            }
        },
        rule() {
            let totals = this.vi.$store.getters.expectedTotalsForCurrentSet
            return totals
        }
    },
    {
        // Матч тотал Меньше
        name: 'TM',
        // 0 Общая игра, 1 Второй сет
        sectionType: 0,
        id: 6,
        timeoutId: 0,
        buttonDataType: 10,
        buttons: [],
        sectionElement: null,
        ordering (a, b) {
            return a.total < b.total;
        },
        processButton(analysis, btn, id) {
            let coef = this.extractCoef(btn);

            let promTotal = this.extractTotal(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];
            let t1 = this.scores[2] ? this.scores[1].reduce((p, c) => p + Number(c), 0) : 0;
            let t2 = this.scores[3] ? this.scores[2].reduce((p, c) => p + Number(c), 0) : 0;
            let nowWinning = lead[0] > lead[1] ? 1 : (lead[0] < lead[1] ? 2 : 0);
            // Сеты начинаются с 1
            let currset = lead[0] + lead[1] + 1;

            let tfor1 = [];
            let tfor2 = [];

            for (t in analysis.ttls) {
                if (analysis.ttls[t].p1 > analysis.ttls[t].p2 && promTotal > (analysis.ttls[t].total + t1 + t2)) {
                    tfor1.push({
                        t: analysis.ttls[t].total + t1 + t2,
                        c: analysis.ttls[t].count,
                    })
                } else if (analysis.ttls[t].p1 < analysis.ttls[t].p2 && promTotal > (analysis.ttls[t].total + t1 + t2)) {
                    tfor2.push({
                        t: analysis.ttls[t].total + t1 + t2,
                        c: analysis.ttls[t].count,
                    })
                }
            }

            // console.log('tfor1')
            // console.log(tfor1)
            // console.log('tfor2')
            // console.log(tfor2)


            tfor1.sort((a, b) => b.c - a.c)
            tfor2.sort((a, b) => b.c - a.c)
            let count = 0;
            if (nowWinning == 1 && tfor1.length) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                minimumCoef = 1 / (count / analysis.g) + grow;


            } else if (nowWinning == 2 && tfor2.length) {
                count = tfor2.reduce((p, c) => { return p + c.c; }, 0)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 0) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                count += tfor2.reduce((p, c) => { return p + c.c; }, count)
                count = count/2
                minimumCoef = 1 / (count / analysis.g) + grow;
            } else {
                return false
            }

            // console.log('--------------')
            // console.log('promTotal', promTotal)
            // console.log('Count', count)
            // console.log('analysis.g', analysis.g)
            // console.log('Chance', count/analysis.g)
            return {
                goodIdea: coef > minimumCoef,
                total: promTotal,
                minimumCoef,
                coef,
                btn,
                chance: (count / analysis.g) * 100
            }
        },
        rule() {
            let totals = this.vi.$store.getters.expectedTotalsForCurrentSet
            return totals
        }

    },
    {
        // Сет 2 тотал Больше
        name: 'S2TB',
        // 0 Общая игра, 1 Второй сет
        sectionType: 1,
        id: 7,
        timeoutId: 0,
        buttonDataType: 9,
        buttons: [],
        sectionElement: null,        
        ordering (a, b) {
            return a.total > b.total;
        },
        processButton(analysis, btn, id) {

            let coef = this.extractCoef(btn);

            let promTotal = this.extractTotal(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];
            if ((lead[0] + lead[1]) > 1) {
                return false
            }
            let t1 = this.scores[2] ? this.scores[1].reduce((p, c) => p + Number(c), 0) : 0;
            let nowWinning = lead[0] > lead[1] ? 1 : (lead[0] < lead[1] ? 2 : 0);
            // Сеты начинаются с 1
            let currset = lead[0] + lead[1] + 1;


            let tfor1 = [];
            let tfor2 = [];

            for (t in analysis.ttls) {
                if (analysis.ttls[t].p1 > analysis.ttls[t].p2 && promTotal < (analysis.ttls[t].total)) {
                    tfor1.push({
                        t: analysis.ttls[t].total + t1,
                        c: analysis.ttls[t].count,
                    })
                } else if (analysis.ttls[t].p1 < analysis.ttls[t].p2 && promTotal < (analysis.ttls[t].total)) {
                    tfor2.push({
                        t: analysis.ttls[t].total + t1,
                        c: analysis.ttls[t].count,
                    })
                }
            }



            tfor1.sort((a, b) => b.c - a.c)
            tfor2.sort((a, b) => b.c - a.c)

            let count = 0;
            if (nowWinning == 1 && tfor1.length) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 2 && tfor2.length) {
                count = tfor2.reduce((p, c) => { return p + c.c; }, 0)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 0) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                count += tfor2.reduce((p, c) => { return p + c.c; }, count)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else {
                return false
            }

            return {
                goodIdea: coef > minimumCoef,
                total: promTotal,
                minimumCoef,
                coef,
                btn,
                chance: (count / analysis.g) * 100
            }
        },
        rule() {
            let totals = this.vi.$store.getters.expectedTotalsForCurrentSet
            return totals
        }

    },
    {
        // Сет 2 тотал Меньше
        name: 'S2TM',
        // 0 Общая игра, 1 Второй сет
        sectionType: 1,
        id: 8,
        timeoutId: 0,
        buttonDataType: 10,
        buttons: [],
        sectionElement: null,
        ordering (a, b) {
            return a.total < b.total;
        },
        processButton(analysis, btn, id) {

            // console.log('Кнопочка', btn)
            let coef = this.extractCoef(btn);

            let promTotal = this.extractTotal(btn);
            let grow = options.growCoef;
            let minimumCoef = 1000;
            let lead = this.scores[0];
            if ((lead[0] + lead[1]) > 1) {
                return false
            }
            let t1 = this.scores[2] ? this.scores[1].reduce((p, c) => p + Number(c), 0) : 0;
            let nowWinning = lead[0] > lead[1] ? 1 : (lead[0] < lead[1] ? 2 : 0);
            // Сеты начинаются с 1
            let currset = lead[0] + lead[1] + 1;

            let w1Chance = Math.round((analysis.wins.p1 / analysis.g) * 100);
            let w2Chance = 100 - w1Chance;

            let tfor1 = [];
            let tfor2 = [];

            for (t in analysis.ttls) {
                if (analysis.ttls[t].p1 > analysis.ttls[t].p2 && promTotal > (analysis.ttls[t].total)) {
                    tfor1.push({
                        t: analysis.ttls[t].total,
                        c: analysis.ttls[t].count,
                    })
                } else if (analysis.ttls[t].p1 < analysis.ttls[t].p2 && promTotal > (analysis.ttls[t].total)) {
                    tfor2.push({
                        t: analysis.ttls[t].total,
                        c: analysis.ttls[t].count,
                    })
                }
            }


            tfor1.sort((a, b) => b.c - a.c)
            tfor2.sort((a, b) => b.c - a.c)
            let count = 0;
            if (nowWinning == 1 && tfor1.length) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 2 && tfor2.length) {
                count = tfor2.reduce((p, c) => { return p + c.c; }, 0)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else if (nowWinning == 0) {
                count = tfor1.reduce((p, c) => { return p + c.c; }, 0)
                count += tfor2.reduce((p, c) => { return p + c.c; }, count)

                minimumCoef = 1 / (count / analysis.g) + grow;

            } else {
                return false
            }
            return {
                goodIdea: coef > minimumCoef,
                total: promTotal,
                minimumCoef,
                coef,
                btn,
                chance: (count / analysis.g) * 100
            }
        },
        rule() {
            let totals = this.vi.$store.getters.expectedTotalsForCurrentSet
            return totals
        }

    },
]

module.exports = function() {
    this.st = [];
    console.log('Xotim sozdat')
    strategies.forEach(d => {
        // console.log('Klonim', d)
        let pushObj = {

        };
        for (v in d) {
            if (typeof d[v] == 'object' && d[v]) {
                if (d[v].length) {
                    pushObj[v] = [...d[v]];
                } else {
                    pushObj[v] = { ...d[v] };
                }
            } else {
                pushObj[v] = d[v]
            }
        }
        this.st.push({ ...pushObj })
    })
};