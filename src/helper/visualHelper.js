import  Vue from  'vue';
import Vuex from 'vuex';

// let Vue = require('vue');
// let Vuex = require('vuex');
let axios = require( 'axios');
let VueAxios = require( 'vue-axios');
let App = require( './App.vue');
let FishUi = require( 'fish-ui');

Vue.use(Vuex);

Vue.use(FishUi);
Vue.use(VueAxios, axios.create({
    baseURL: `http://93.76.249.123:3000`,
    withCredentials: false,
    headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Origin': '1'
    }
}));

// module.exports = { Vue, Vuex, App }

// export default Vue
export  {
	Vue,
	Vuex,
	App
}