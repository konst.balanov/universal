if (!localStorage.bettts) {
    localStorage.bettts = JSON.stringify([]);
}

function getTrackkkedMatch(matchId) {
    let trackkked = JSON.parse(localStorage.trackkks);
    if (trackkked[matchId]) {
        return trackkked[matchId]
    } else {
        return false
    }
    // if (trackkked.indexOf(matchId) == -1) return false
    // else return true
}


function saveTrackkkedMatch(matchId, trackL, trackM) {
    let trackkked = JSON.parse(localStorage.trackkks);
    if (!trackkked[matchId]) {
        trackkked[matchId] = {}
    }
    if (trackL)
        trackkked[matchId].trackL = trackL.map();
    if (trackM)
        trackkked[matchId].trackM = trackM;
    localStorage.trackkks = JSON.stringify(trackkked);
}

function saveBettedMatch(matchId) {
    let betted = JSON.parse(localStorage.bettts);
    betted.push(matchId)
    localStorage.bettts = JSON.stringify(betted);
}

function doesBetted(matchId) {
    let betted = JSON.parse(localStorage.bettts);
    if (betted.indexOf(matchId) == -1) return false
    else return true
}

function saveToStorage(name, value, cb) {
    let obj = {};
    obj[name] = value


    chrome.storage.local.set(obj, function(respo) {
        if (cb)
            cb()
    })
}

function getFromStorage(name, cb) {
    chrome.storage.local.get(name, function(result) {
        if (cb) {
            cb(result);
        }
    })
}

module.exports = {
    getTrackkkedMatch,
    saveTrackkkedMatch,
    saveBettedMatch,
    doesBetted,
    saveToStorage,
    getFromStorage
}