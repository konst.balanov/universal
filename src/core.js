let $ = require('jquery');

let {options, updateOptions} = require('./options.js');

let stopAfterBet = {
    s: false
}

let {
    getTrackkkedMatch,
    saveTrackkkedMatch,
    saveBettedMatch,
    doesBetted,
    saveToStorage,
    getFromStorage
} = require('./storage.js');

var matches = null;
var watchMatch = null;
var timeOuts = null;
var matchesToUse = null;
var ignoredMatches = null;

function pressButton(button, additionalDelay = false, zerodelay = false) {
    if ($(button).hasClass('c-events__more')) {
        // console.trace()
    }
    var evt = new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window
    });

    let delay = Math.random()*1000+Math.random()*1000*2 + additionalDelay ? 3430 : 124;

    if (zerodelay) {
        delay = 371;
    }

    if (button && button.dispatchEvent) {

        setTimeout(function () {
            var canceled = !button.dispatchEvent(evt);
        }, delay)
        return true;
    } else {
        return false
    }
}

let getMatchId = function(match) {
    let ref = $(match).find('.c-events__to-top+a');
    let str = $(ref).attr('href')
    let ars = ''
    if (str) {
        ars = str.match(/[\d]{4,14}/g);
    } else {
        return 0;
    }
    let id = ars[1];
    let leagueId = ars[0];
    return [id, leagueId];
}

function betNotInProg() {
    if ($('.coupon__preloader').css('display') == 'none') {
        return true;
    } else {
        return false;
    }
}


function validateLeague(match) {
    // console.log('Я снова живу')
    // return true;
    let champblock = $(match).parent().parent();
    let header = $(champblock).find('.c-events__item_head .c-events__liga')[0]
    let fullleague = header.innerText;
    if (fullleague.includes('18')) {
        return true
    } else {
        return false
    }
}


function ignoreMatch(matchId) {
    ignoredMatches.push(matchId);
}

// function searchMatches() {
//     console.log('Searching matches')
//     $('div:not(.ignoreme)>.c-events__item.c-events-scoreboard__wrap:not(.c-events__item_col, .c-events__item_head)').each(processMatch)
//     // sendMatchesToExt(matches);
// }

function isMatchIgnored(matchId) {
    if (ignoredMatches.indexOf(matchId) == -1) {
        return false;
    } else {
        return true;
    }
}


const COLORS = {
    TOOLATE: 'gainsboro',
    BETTED: 'lightblue',
    AFTER5: 'hotpink',
    INVALID: '#fbcbcb'
}

function markAs(match, clr) {
    // $(match).css('background', clr);
}


function matchSportId(match) {
    return Number($('.c-events__item.c-events__item_head svg.icon use', match.closest('[data-name=dashboard-champ-content]')).attr('xlink:href').match(/\d+$/)[0])
}

function processMatch(index, match) {

    let matchId = getMatchId(match)[0];

    if (isMatchIgnored(matchId)) {
        // console.log(1)
        return
    }

    let quater = extractQuater(match);

    if (isNaN(quater)) {
        return
    }

    let sportId = matchSportId(match);
    if (!([89, 215, 130].includes(sportId))) {
        ignoreMatch(matchId, match)
        return false
    }
    // console.log(sportId, 'GGGGGGGGG')

    if (sportId == 89 && !validateLeague(match)) {
        ignoreMatch(matchId, match);
        return false
    }



    // if (doesBetted(matchId)) {
    //     markAs(match, COLORS.BETTED)
    //     // console.log(3)
    //     ignoreMatch(matchId, match);
    //     return
    // }


    // if (matchSportId(match) != 158) {
    //     ignoreMatch(matchId, match);
    //     return false
    // }

    let started = false;

    let s1s = $('.c-events-scoreboard__cell.c-events-scoreboard__cell--all', match).get().map(function (b) {
        return Number($(b).text().trim())
    })

    if (matchesToUse.indexOf(matchId) != -1) {
        // console.log(2)
        return
    }

    // console.log('Will use this match', match)
    matchesToUse.push(matchId);
    if (matches.length < 120) {
        matches.push(new watchMatch(match, matchId, sportId));
        // console.log(matches)
    }
    // console.log(matches)
};


function extractQuater(match) {
    return parseInt($(match).find('.c-events__overtime').text().split(' ')[0])
}

function extractMatchName(match, separate) {
    let teams = $(match).find('span.c-events__teams .c-events__team').get();
    // console.log('Extracting matchname')
    let opps = [];

    

    let name = teams.reduce(function(matchName, current, index) {
        let curropp = $(current).text().trim();
        if (separate) {
            opps.push(curropp);
        }
        return matchName + curropp + (index == 0 ? ' - ' : '')
    }, '')
    if (separate) {
        return opps;
    }
    return name
}

module.exports = function(matchesQ, matchesToUseQ = 0, ignoredMatchesQ = 0, timeOutsQ = 0) {
    if (!matchesToUseQ) {
        watchMatch = matchesQ
        return
    }
    matches = matchesQ;
    matchesToUse = matchesToUseQ;
    ignoredMatches = ignoredMatchesQ;
    timeOuts = timeOutsQ;
    return {
        pressButton,
        getMatchId,
        betNotInProg,
        validateLeague,
        ignoreMatch,
        isMatchIgnored,
        markAs,
        processMatch,
        extractQuater,
        extractMatchName,
        $,
        stopAfterBet
    }
};