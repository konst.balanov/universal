import Vue from 'vue';
import App from './App.vue';
import FishUi from 'fish-ui';
import Vuex from 'vuex';

Vue.use(FishUi);
Vue.use(Vuex)

let store = new Vuex.Store({
    state: {
        matches: [],
        schedule: [],
        botId: '',
        botName: '',
        nowWorking: false,
        options: {
            maximumCoef: 1.9,
            minimumCoef: 1.35,
            minimumChance: 0,
            maximumChance: 75,
            growCoef: 0.3,
            showHelper: true,
            autoStart: false
            // showHelper: false
        }
    },
    getters: {
        matchesList(state) {
            return state.matches
        },
        getOptions(state) {
            return state.options
        },
        botId(state) {
            return state.botId
        },
        botName(state) {
            return state.botName
        },
        nowWorking(state){
            return state.nowWorking
        },
        schedule(state){
            return state.schedule
        },
    },
    mutations: {
        toggleNowRunning(state) {
            state.nowWorking = !state.nowWorking
        },
        updateOptions(state, options) {
            let oldOp = state.options;
            state.options = Object.assign({}, oldOp, options);
            state.nowWorking = options.autoStart;
        },
        updateBotId(state, newId) {
            state.botId = newId;
        },        
        updateBotName(state, newName) {
            state.botName = newName;
        },        
        updateSchedule(state, schedule) {
            let oldOp = state.schedule;
            state.schedule = Object.assign({}, oldOp, schedule);
        },
        updateMatches(state, matches) {
            state.matches = matches;
        },
        updateStatus(state, status) {
            state.nowWorking = status
        }
    }
})

var vm = new Vue({
    el: '#vue-app-one',
    render: h => h(App),
    store,
    data: function() {
        return {
            title: 'Test',
            matches: [{ matchId: 1 }, { matchId: 2 }, { matchId: 3 }]
        }
    },
    mounted() {

    }
});

function sendToApp(name, value) {
    console.log('sendToApp', name, value)
    vm.$store.commit('update' + name.charAt(0).toUpperCase() + name.slice(1), value);
}

function extractValue(param) {
    // console.log('extractValue', param)
    chrome.storage.local.get(param, function(result) {
        console.log('Dostalo', param, result[param])
        sendToApp(param, result[param]);
    })
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.type == 'readyToExtract') {
        extractValue('matches')
    }
})

extractValue('matches')
extractValue('options')
extractValue('status')
extractValue('botId')
extractValue('botName')
extractValue('schedule')