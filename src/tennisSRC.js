let $ = require('jquery');
let allowstart = false;


// let history = require('./history.js');
let {options, updateOptions} = require('./options.js');

let {matches, matchesToUse, ignoredMatches, timeOuts} = require('./matchesStorage.js');
let { getTrackkkedMatch, saveTrackkkedMatch, saveBettedMatch, doesBetted, saveToStorage, getFromStorage } = require('./storage.js');

let core = require('./core.js')(matches, matchesToUse, ignoredMatches, timeOuts);

let pageWalker = require('./pageWalker.js');
pageWalker.use($);
pageWalker.use(core.pressButton);

// let botManager = require('./botManager.js')

let { watchMatch, searchMatches } = require('./custom.js')(core, matches, matchesToUse, ignoredMatches, timeOuts, pageWalker, stopScanning)

require('./core.js')(watchMatch)

let historyStorage = [];


var tostop = 0;
var toreload = 0;
var tostopsend = 0;

function stopScanning() {
    let stopScaningId = setInterval(function() {
        if (!matches.length) {
            clearInterval(stopScaningId)
            return
        }
        let match = matches.shift();
        match.killMe();
    }, 74)

    clearInterval(tostop) 
    tostop = 0;
    clearInterval(tostopsend)
    clearInterval(toreload)

    updateRunning()
}

console.log('Паехале')

function startScanning() {

    chrome.runtime.sendMessage({ type: 'checkifallow' }, function(response) {});

    if (location.pathname.indexOf('live') != -1 && !tostop) {
        matchesToUse = [];
        tostop = setInterval(searchMatches, 5000);
        updateRunning()
        searchMatches();
    }
}

function updateRunning(is) {
    chrome.storage.local.set({status: tostop}, function () {
        updateRunning()
    })
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.type == 'stopScan') {
        console.log('stopScanning message')
        stopScanning();
    }

    if (request.type == 'allowstart') {
        allowstart = request.allow
        if (!request.allow) {
            stopScanning();
        }
    }

    if (request.type == 'startScan') {
        console.log('startScanning message')
        startScanning();
    }
    if (request.type == 'startScheduledAction') {
        switch (request.plantype) {
            case 0: console.log('Делаем рандомный теннис')
                    pageWalker.addSportTolive(158);
                    pageWalker.randomBet(15821)
                    break;
            case 1: console.log('Делаем победный теннис')
                    pageWalker.getCurrentLives().forEach(pageWalker.removeSportFromLive)
                    pageWalker.addSportTolive(158);
                    core.stopAfterBet.s = true;
                    setTimeout(startScanning, 9120)
                    break;
            case 2: console.log('Делаем рандомную ставку')
                    let sp1 = Math.floor(Math.random()*216 + 1), sp2 = Math.floor(Math.random()*216 + 1), 
                        sp3 = Math.floor(Math.random()*216 + 1), sp4 = Math.floor(Math.random()*216 + 1);
                    // console.log('В спортах', sp1, sp2, sp3)
                    pageWalker.addSportTolive(sp1); pageWalker.addSportTolive(sp2); pageWalker.addSportTolive(sp3); pageWalker.addSportTolive(sp4);
                    pageWalker.randomBet(10000)
                    break;
        }
        console.log('Попался ивент startScheduledAction', request)
    }

    if (request.type == 'updateOptions') {
        updateOptions(request.options)

        // options = request.options
        // saveOptions({ coef: request.newCoef, stock: request.newStock });
    }
    if (request.type == 'removeHelpers') {
        // console.log('Кукусики')
        matches.forEach(m => m.justRemoveHelper.call(m))
    }    
    if (request.type == 'showHelpers') {
        // console.log('Кукусики2')
        matches.forEach(m => m.remountApp.call(m))
    }
})
window.onload = function () {
    if (options.autoStart) {
    	startScanning()
    }
}
