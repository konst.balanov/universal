// let history = require('./history.js');
let $ = require('jquery');
let { saveBettedMatch, doesBetted } = require('./storage.js');
let strategies = require('./strategies.js');
let { options, updateOptions } = require('./options.js');
let { sendMatchesToExt } = require('./extTalk.js')();

let vued = require('./helper/visualHelper.js');
let Vue = vued.Vue
let Vuex = vued.Vuex
let App = vued.App.default

let stopAfterBet;

var matches;
var matchesToUse;
var ignoredMatches;
var timeOuts = [];

var pressButton;
var getMatchId;
var betNotInProg;
var validateLeague;
var ignoreMatch;
var isMatchIgnored;
var markAs;
var Sound;
var processMatch;
var extractQuater;
var extractMatchName;
var pageWalker;
var stopScheduled;



let historyStorage = [];


function searchMatches() {
    console.log('Searching matches')
    $('div:not(.ignoreme)>.c-events__item.c-events-scoreboard__wrap:not(.c-events__item_col, .c-events__item_head)').each(function(ind, match) {
        if (processMatch(ind, match)) {
            let matchId = getMatchId(match)[0];
            matchesToUse.push(matchId);
            if (matches.length < 160) {
                matches.push(new watchMatch(match));
                console.log(matches)
            }
        }
    })

    // sendMatchesToExt(matches);
}

function splitScores(score) {
    let obj = {

    };

    function extractScores(sc) {
        let obj = {};
        let g = sc.split(':');
        obj.h = g[0];
        obj.g = g[1];
        return obj
    }
    console.log(score)
    let s = score.split(' (');
    s[1] = s[1].split(')')[0];
    let se = s[1].split(',');
    let ress = se.map(function(r) {
        return extractScores(r);
    })
    obj.h = s[0].split(':')[0];
    obj.g = s[0].split(':')[1];

    obj.r = ress
    return obj;
}

let extendWatch = {
    mountApp() {
        // return
        let od = 'od' + this.matchId;
        let obj = options.showHelper ? $('.c-events-scoreboard__lines', this.match).before(`<div id="${od}"><span>${od}</span></div>`).prev()[0] : false;
        let mObj = options.showHelper ? $(`#${od} span`, this.match)[0] : false
        let mtchInst = this;
        let stata = this.stata;
        let name = this.matchName;
        let mI = this;
        this.obj = options.showHelper ? obj : false;
        const store = new Vuex.Store({
            strict: true,
            state: {
                stata: stata,
                matchName: name,
                scores: this.scores || [],
                chances: this.chances,
                w1: 0,
                w2: 0,
                // scores: [
                //     [0, 0],
                //     [],
                //     [],
                //     []
                // ],
                factQuoter: 0
                // mi: mI
            },
            mutations: {
                setFactQuoter(state, payload) {
                    state.factQuoter = payload.quoter;
                },
                setScoreForSet(state, payload) {
                    state.scores[payload.set] = payload.payload;
                    state.scores = [...state.scores]
                    console.log(state.scores)
                },
                updateChances(state, payload) {
                    state.chances = payload.newChances;
                    state.chances = [...state.chances];
                    state.w1 = payload.w1;
                    state.w2 = payload.w2;
                }
            },
            actions: {

            },
            getters: {
                getChances(state) {
                    return state.chances
                },
                // SHOW && TRACK
                getStata(state) {
                    return state.stata
                },
                getName(state) {
                    return state.matchName
                },
                getFactQuoter(state) {
                    return state.factQuoter
                },
                expectingTotal() {
                    let currscore = this.data[0].split(":")
                    currscore = Number(currscore[0]) + Number(currscore[1]);
                    return Number(this.getCurrentTotal) + currscore;
                },
                currentSet: state => {
                    return state.scores[0][0] + state.scores[0][1]
                },
                getScores: state => {
                    return state.scores
                },
                getSetScore: state => set => {
                    // return state.scores[set][0] ? true : false
                    // return state.scores[set][0] ? state.scores[set][0] + ":" + state.scores[set][1] : false
                    return (state.scores[set] && state.scores[set].length) ? [state.scores[set][0], state.scores[set][1]] : false
                },
                // Матчи следующего сета, у которых сет set отыгран со счетом из scores[set]
                getCurrentTotal: (state, getters) => {
                    let lastSet = getters.currentSet;
                    if (lastSet != 0) {
                        // return state.score
                        return state.scores[lastSet][0] + state.scores[lastSet][1] + (lastSet > 1 ? state.scores[1][0] + state.scores[1][1] : 0)
                    } else {
                        // return 0
                    }
                },
                stataBasedOnSet: state => set => {
                    return state.stata.filter(function(s) {
                        return (s.r[set - 1].h == state.scores[set][0] && s.r[set - 1].g == state.scores[set][1] && (set > 1 ? s.r[0].h == state.scores[1][0] && s.r[0].g == state.scores[1][1] : true))
                    })
                },
                matchName: state => state.matchName,

                // PREDICTIONS
            }
        })
        console.log(stata)

        this.vi = new Vue({ el: options.showHelper ? mObj : null, render: h => h(App), store })
    },

    // gameSet должен быть 1, 2; 0 - общий счёт
    extractScore(gameSet = 0) {
        let ret = [];

        let selector = gameSet ? '.c-events-scoreboard__cell:not(.c-events-scoreboard__cell--all)' : '.c-events-scoreboard__cell.c-events-scoreboard__cell--all';
        $(this.match).find('.c-events-scoreboard__lines .c-events-scoreboard__line').each(function(index, score) {
            $(score).find(selector).each(function(ind, sco) {
                if (gameSet) {
                    if ((1 + ind) == gameSet) {
                        ret.push(parseInt($(sco).html()));
                    }
                } else {
                    ret.push(parseInt($(sco).html()));
                }
            })
        })
        return ret
    },
    stopStrategy(stId) {
        if (this.strategies)
            this.strategies.forEach(function(d) {
                if (d.id == stId) {
                    deleteCustomInterval(d.timeoutId)
                }
            })


    },

    stopAllStrategies() {
        console.log('Stoppim strati')
        if (this.strategies)
            this.strategies.forEach(s => {
                deleteCustomInterval(s.timeoutId);
                s.timeoutId = 0;
            })
    },
    makeBet(candidate) {
        // return
        if (betNotInProg() && !this.betIsLocked(candidate)) {
            let bettod = pressButton(candidate, false, true);
            if (bettod) {
                saveBettedMatch(this.matchId);
                this.stopAllStrategies.call(this);
                if (stopAfterBet.s) {
                    stopScanning();
                }
                // pageWalker.addSportTolive('Basketball')
                // pageWalker.
            }
            // console.log('Убили после совершения ставки')
            // this.killMe.call(this);
        }
    },
    closeMoreEvents() {
        let btns = $('.active', this.match)
        if (btns[0]) {
            pressButton(btns[0])
        }
        if (btns[1] && !this.foldedS2) {
            pressButton(btns[1], true)
        }
    },
    extractTotal(x) {
        return parseFloat($(x).text().split(' ')[1])
    },
    extractCoef(x) {
        return parseFloat($(x).next().attr('data-coef'))
    },
    nodeIsActual(node, set = 0) {
        if (set) {
            // console.log('Checking node', node, set)
        }
        let l = $.contains(document, node)
        return l
    },
    updateBetButtons(buttontype, section, strategyname) {
        let found = $('span[data-type="' + buttontype + '"]', section).get();
        this.buttons[strategyname + 'buttons'] = found;
        return found;

    },
    betIsLocked(button) {
        return $(button).parent().hasClass('blockSob')
    },
    searchGoodButtons(strategy) {

        let analysis = strategy.rule.call(this)
        // console.log(analysis)
        // let gb = strategy.buttons.filter(strategy.processButton.bind(this, analysis))
        let gb = strategy.buttons.get().map(strategy.processButton.bind(this, analysis)).filter(function(r, d) {
            // console.log(r)
            // console.log('Шо уже не так блять', r)
            // console.log('Опции', options)
            switch (r) {
                case false:
                    // console.log('Ebob', r)
                    return false;
                default:
                    {
                        // console.log('BEB', r, options)
                        if (!r.goodIdea) { /*console.log('Я1');*/ return false };
                        if (r.coef < options.minimumCoef || r.coef > options.maximumCoef) { /*console.log('Я2');*/ return false };
                        if (r.chance < options.minimumChance || r.chance > options.maximumChance) { /*console.log('Я3');*/ return false };
                        // console.log('Я нахуй должен добавиться', r)
                        return r.goodIdea;
                    }
            }
        })
        // gb.sort(strategy.ordering)
        // console.log(strategy.sectionElement, this.matchName, strategy.name, strategy.timeoutId)
        if (gb.length) {
            console.log("Кандидаты", strategy.name)
            console.log(gb)
            // console.log(strategy.ordering)
            // gb.get().forEach(function (a) {
            //     console.log(a.btn, a.chance, a.minimumCoef)
            // })
            if (strategy.name == 'TM' || strategy.name == 'S2TM') {
                console.log(gb[gb.length - 1].btn)
                return gb[gb.length - 1].btn
            } else {
                console.log(gb[0].btn)
                return gb[0].btn;
            }
        }

        // return false
    },
    matchInList(mtch) {

        if (mtch && !(matches.some(m => mtch.matchId == m.matchId))) {
            console.log('Убили несуществуюющую хрень', mtch)
            mtch.killMe.call(mtch);
        }
    },
    waitForCoef(strategy) {
        // console.log('Вот он wfc')
        // console.log("При", strategy.timeoutId);
        if (!strategy.timeoutId) {
            strategy.timeoutId = customInterval(function() {
                // console.log("Работает страта", strategy.name, this.matchName)

                if (this.nodeIsActual(strategy.sectionType ? this.s2Section : this.wholeSection)) {
                    if ($(strategy.sectionType ? this.s2Section : this.wholeSection).is(strategy.sectionType)) {

                    } else {
                        strategy.sectionElement = strategy.sectionType ? this.s2Section : this.wholeSection
                        // return
                    }
                }

                this.updateBetButtons.call(this, strategy);

                let candidate = this.searchGoodButtons.call(this, strategy);

                if (candidate) {
                    // console.log("Кандидат", candidate);
                    this.makeBet.call(this, candidate);
                } else {
                    // this.killMe.call(this);
                }
            }.bind(this), 1600, { name: 'Wait for coef for ' + this.matchName });
        } else {
            // console.log('Крыса хотела перезаписать айди для страты', strategy, "в", this.matchName)
        }

    },

    killMe(runagain = false, temp = false) {
        console.log('Want to delete self', this.matchName)
        // console.trace()
        deleteCustomInterval(this.validateMatchNodeId);
        deleteCustomInterval(this.watchTotalScoresId);
        deleteCustomInterval(this.watchSetScoresId);
        this.analysisTimers.forEach(at => deleteCustomInterval(at.id))

        // console.log(this.strategies)
        if (this.strategies)
            this.strategies.forEach(function(st) {
                deleteCustomInterval(st.timeoutId)
            })
        // console.log(this.strategies)
        // console.log(this)
        // console.log(matches)

        // if (!temp) {
        // console.log('And closing')
        this.closeMoreEvents.call(this)
        // }
        this.vi.$destroy();
        if (this.obj) {
            this.obj.remove();
        }

        let curmId = matches.findIndex(m => this.matchId == m.matchId)
        matches.splice(curmId, 1);
        // console.log(matches)
        matchesToUse.splice(matchesToUse.indexOf(this.matchId), 1);

        // console.log(matches)


        // for (var i = matches.length - 1; i >= 0; i--) {
        //     if (matches[i].matchId == this.matchId) {
        //         matches.splice(i, 1);
        //         matchesToUse.splice(matchesToUse.indexOf(this.matchId), 1);
        //     }
        // }

        if (runagain) {
            searchMatches();
            // this.closeMoreEvents.call(this)
        }
        // sendMatchesToExt(matches);
    },
    findMatchById(id) {
        let ret = null
        $('div>.c-events__item.c-events-scoreboard__wrap:not(.c-events__item_col, .c-events__item_head)').filter(function(dd, mtch) {
            let schid = getMatchId(mtch)[0];
            if (schid && schid == id) {
                ret = mtch
                return mtch
            }
        });
        return ret;
    },
    updateMatchNode() {
        // console.log()
        // console.log("Матч нода не актуальна, обновим")
        let newel = this.findMatchById.call(this, this.matchId);
        if (newel) {
            // console.log(newel)
            this.match = newel;
            this.remountApp.call(this)
            // this.killMe.call(this, true, true);
        } else {
            console.log('Не нашли как апдейтнуть меин ноду для', this.matchName)
            // this.killMe.call(this);
        }
    },
    validateMatchNode() {
        if (this.nodeIsActual(this.match)) {

            if (this.nodeIsActual(this.wholeSection, 1)) {
            } else {
                // this.wholeIsOpened
                this.updateWholeSection.call(this)
            }

        } else {
            this.updateMatchNode.call(this)
        }
    },
    watchScore(set = 0) {
        let v = this.extractScore(set);
        if (!this.scores[set]) {
            this.scores[set] = [];
        }
        // console.log(set, '---', v)
        let factQuoter = extractQuater(this.match);

        this.setFactQuoter.call(this, factQuoter);

        if (typeof this.scores[set][0] == 'undefined' || typeof this.scores[set][1] == 'undefined') {
            console.log('Потому-что undefined')
            this.updateScores.call(this, set, v)
        }

        if (v[0] > this.scores[set][0] || v[1] > this.scores[set][1]) {
            console.log('Потому-что больше')
            this.updateScores.call(this, set, v)
        }


        if (isNaN(factQuoter)) {
            console.log('Килляюсь чи шо')
            this.killMe.call(this)
            return
        }


        if (set != 0 && set != extractQuater(this.match)) {
            deleteCustomInterval(this.watchSetScoresId);
            console.log('Скипаем сет, го некст')
            this.watchSetScoresId = customInterval(this.watchScore.bind(this, set + 1), 1450, {
                name: 'Watch ' + (set + 1) + ' set score for ' + this.matchName
            });
        }
    },
    updateScores(set, payload) {
        this.scores[set] = payload;

        if (this.vi) {
            if (set != 0 && this.factQuoter == set) {
                console.log('Получалка', this.sportId, this.opp1, this.opp2, this.scores, this.factQuoter)
                getMatchAnalysis.call(this, this.sportId, this.opp1, this.opp2, this.scores, this.factQuoter);
            }
            this.vi.$store.commit('setScoreForSet', { set: set, payload: payload });
        }
    },
    setFactQuoter(fQ) {
        this.factQuoter = fQ;
        if (this.vi) {
            this.vi.$store.commit('setFactQuoter', {
                quoter: fQ
            })
        }
    },
    destroyHelper() {
        console.log('destroyHelper')
        if (this.vi) {
            this.vi.$destroy();
        }
        if (this.obj) {
            console.log('destroyObj')
            this.obj.remove();
        }
    },
    justRemoveHelper() {
        console.log('justRemoveHelper')
        if (this.obj && (this.obj.remove instanceof Function)) {
            this.obj.remove();
            this.obj = null;
        }
    },
    remountApp() {
        console.log('remountApp')
        this.destroyHelper.call(this)
        this.mountApp.call(this);
        if (this.scores[0])
            this.updateScores(0, this.scores[0])
        if (this.scores[1])
            this.updateScores(1, this.scores[1])
        if (this.scores[2])
            this.updateScores(2, this.scores[2])
        if (this.scores[3])
            this.updateScores(3, this.scores[3])
        if (this.scores[4])
            this.updateScores(4, this.scores[4])
    },
    // BETTER
    updateS2Section() {
        if (this.moreSetsOpened) {
            if (this.moreSetsIsOpened()) {
                if (this.s2Opened && this.nodeIsActual(this.moreSetsSection)) {
                    this.s2Section = this.findSetXSection.call(this, 2);
                } else {
                    if (this.moreSetsSection) {
                        if (this.nodeIsActual(this.moreSetsSection)) {
                            this.openXSet.call(this)
                        } else {
                            this.findMoreSetsSection.call(this)
                        }
                    } else {
                        this.findMoreSetsSection.call(this)
                    }
                }
            } else {}
        } else {
            this.openMoreSets();
        }
    },
    updateWholeSection() {
        if (this.wholeOpened) {
            if (this.wholeIsOpened()) {
                this.wholeSection = this.findWholeSection.call(this)
            }
        } else {
            console.log('Не открыто по этому открываем')
            this.openWholeSection()
        }
    },
    wholeIsOpened() {
        let wholeSelector = '.c-events__more.c-events__more_bets';
        let wholeButton = $(wholeSelector, this.match)[0]
        return $(wholeButton).hasClass('active');
    },
    moreSetsIsOpened() {
        let moreSetsSelector = '.c-events__more.c-events__more_events';
        let moreSetsOpenedButton = $(moreSetsSelector, this.match)[0]
        return $(moreSetsOpenedButton).hasClass('active')
    },
    setXIsOpened() {
    },
    openWholeSection() {
        let wholeSelector = '.c-events__more.c-events__more_bets';
        let wholeButton = $(wholeSelector, this.match)
        if (!this.wholeOpened && wholeButton.length) {
            this.wholeOpened = true;
            pressButton(wholeButton[0])
        }
    },
    openMoreSets() {
        let moreSetsSelector = '.c-events__more.c-events__more_events';
        let moreSetsOpenedButton = $(moreSetsSelector, this.match)
        // console.log("Да-да открываем")
        // console.log("Сейчас доп. сеты", this.moreSetsOpened ? "открыты" : "закрыты")
        if (!this.moreSetsOpened && moreSetsOpenedButton.length) {
            this.moreSetsOpened = true;
            pressButton(moreSetsOpenedButton[0])
        }
    },
    findMoreSetsSection() {
        let sib = $(this.match).siblings('.c-events.c-events_inner');
        this.moreSetsSection = sib.length ? sib[0] : null
        // console.log(this.moreSetsSection)
        return sib.length ? sib[0] : null;
    },
    openXSet(set) {
        if (!this.s2Opened && this.moreSetsOpened && this.moreSetsSection) {
            let setsection = this.findSetXSection.call(this, 2);
            let mor = $('.c-events__more-wrap a:not(.active)', setsection)[0]
            pressButton(mor, true);
            this.s2Opened = true;
        }
    },
    findWholeSection() {
        let next = $(this.match).siblings('.c-events__moreEvs');
        if (next.length) {
            return next[0]
        }
    },
    findSetXSection(set) {
        // let setX = $(this.match).siblings('.c-events.c-events_inner');
        let setSection = $('>div', this.moreSetsSection).get().filter(function(sd) {
            return $(sd).text().trim()[0] == set
        });
        return setSection ? setSection[0] : null;
    },
    closeWholeSection() {
        let wholeSelector = '.c-events__more.c-events__more_bets.active';
        let wholeButton = $(wholeSelector, this.match)
        if (wholeButton.length && this.wholeOpened) {
            pressButton(wholeButton)
            this.wholeOpened = false;
        }
    },
    closeMoreSetsSection() {
        let btn = $('.c-events__more.c-events__more_events.active', this.match)
        if (btn[0] && this.moreSetsOpened) {
            pressButton(btn[0]);
            this.moreSetsOpened = false;
        }
    },
    setTitleWithFinishScore(score) {
        let titler = $('.c-events__teams', this.match);
        // console.log(titler)
        titler.attr('title', score)
    },
    placeMarkers(pointer) {
        // console.log('Запущен поинтер', pointer.name)
        if (pointer['data-type'] == 9889999) {
            // console.log(9889999, this.accScore)
            if (this.accScore && this.accScore.length == 1) {
                this.setTitleWithFinishScore(this.accScore[0].score);
            } 
            // else if (this.accScore && this.accScore.length > 1) {
                // if (typeof this.accScore[0].score) {
                    // let scoresChance = this.setTitleWithFinishScore(this.accScore[0].score.reduce(function (acc, val) {
                    //     return acc += ` ${val.chance}%: ${val.score} |`
                    // }, "")    );
                // }
                // this.setTitleWithFinishScore(scoresChance);

            // }
            // return
        } 

        let marker_btns = this.updateBetButtons(pointer['data-type'], this.wholeSection, pointer.name);
        let promTotal = 0;
        let calcedChance = 0;
        marker_btns.forEach(b => {
            switch(pointer['data-type']) {

                case 9: 
                     promTotal = this.extractTotal(b);
                    if (!this.chances.length) {
                        // this.removeAttached(b)
                        // return
                    }
                    calcedChance = this.chances.find(chance => chance.total == promTotal || (chance.more == 100 &&  chance.total > promTotal))
                    if (calcedChance) {
                        this.attachText(b, calcedChance.more);
                    } else {
                        // this.removeAttached(b)
                    }
                    break;

                case 10: 
                    promTotal = this.extractTotal(b);
                    if (!this.chances && !this.chances.length) {
                        // console.log('Нет шансов для W1', this.matchName)
                        // this.removeAttached(b)
                        // return
                    }
                    calcedChance = this.chances.find(chance => chance.total == promTotal  || (chance.less == 100 &&  chance.total < promTotal))
                    if (calcedChance) {
                        this.attachText(b, calcedChance.less);
                    } else {
                        // this.removeAttached(b)
                    }
                    break;
                case 401:
                    if (this.w1Chance) {
                        // return
                    }
                    this.attachText(b, this.w1Chance);
                    break;
                case 402:
                    if (this.w2Chance) {
                        // return
                    }
                    this.attachText(b, this.w2Chance);
                    break;

                case 1:
                    if (this.ww1Chance) {
                        // return
                    } else {
                        // this.removeAttached(b)
                        return
                    }
                    this.attachText(b, this.ww1Chance);
                    break;
                case 3:
                    if (this.ww2Chance) {
                        // return
                    } else {
                        // this.removeAttached(b)
                        return
                    }
                    this.attachText(b, this.ww2Chance);
                    break;
                case 2:
                    if (this.ww0Chance) {
                        // return
                    } else {
                        // this.removeAttached(b)
                    }
                    this.attachText(b, this.ww0Chance);
                    break;

                case 731: 
                    let promAS = $(b).text()
                    if (!this.accScore.length) {
                        return
                    }
                    calcedChance = this.accScore.find(score => promAS.includes(score.score))
                    if (calcedChance) {
                        this.attachText(b, calcedChance.chance);
                    } else {
                        // this.removeAttached(b)
                    }
                    break;
                case 759: 
                    if (this.OTYChance) {
                        // return
                    } else {
                        // this.removeAttached(b)
                    }
                    this.attachText(b, this.OTYChance);
                    break;
                case 761: 
                    if (this.OTNChance) {
                        // return
                    } else {
                        // this.removeAttached(b)
                    }
                    this.attachText(b, this.OTNChance);
                    break;
                
            }
        })
    },
    attachText(button, percents) {
        let currcontent = $(button).text();
        let appendorput = $('span', button).get()
        if (percents == null) {
            return
        }
        if (appendorput.length) {
            $(appendorput[0]).text(`${percents}%`);
        } else {
            $(button).append(`<span class='marker' style="left:75%; position:absolute;font-size:14px;">${percents}%</span>`)
        }
        $(button).css('background', `rgba(${255-255*(percents/101)}, ${255*(percents/101)}, 0, 0.1)`)
        // $(button).text(currcontent.replace(/(\d+\%)*$/, percents + '%'))
    },
    removeAttached(button) {
        $(button).css('background', `none`)
        let appendorput = $('span', button).get();
        if (appendorput.length) {
            $(appendorput[0]).remove()
        }
    },
    clearAllButtons() {
        // alanysisStrategies.forEach(st => {
        $('[data-type]', this.wholeSection).get().forEach(this.removeAttached)
        // })

    }
}

function matchSportId(match) {
    return Number($('.c-events__item.c-events__item_head svg.icon use', $(match).closest('[data-name=dashboard-champ-content]')).attr('xlink:href').match(/\d+$/)[0])
}

function scoreQueryBuilder(score, fQ) {
    let retval = "\\("
    // return retval;
    console.log('Строим счет по строке', score, fQ)
    for (var i = 1; i <= fQ; i++) {
        if (typeof score[i][0] == 'undefined' || typeof score[i][1] == 'undefined') {
            return false
        }
        if (fQ == i) {
            retval += '!' + score[i][0] + ':' + '!' + score[i][1];
        } else {
            retval += score[i][0] + ':' + score[i][1];
        }
        if (i != fQ) {
            retval += ','
        }
    }
    console.log('Такой retval у нас', retval)
    return retval;
    // return score[1].join()
}



function getMatchAnalysis(sportid, team1, team2, scorequery, fQ) {
    let scoreQuery = scoreQueryBuilder(scorequery, fQ);
    if (!scoreQuery) {
        console.log('Сайт не показывает счет для', team1, team2)
        return false;
    }
    chrome.runtime.sendMessage({
        type: 'gethistory',
        sportid,
        team1,
        team2,
        scorequery: scoreQuery

    }, follow.bind(this))
}

function follow(stata) {
        console.log('FOLLOWING', stata)
        this.clearAllButtons()
        if (!stata || Object.keys(stata).length == 0) {
            console.log('no mi zdes taki')
            return
        }
        if (stata) {
            console.log('statu', stata)
            this.chances = stata.totals;
            this.accScore = stata.probablyscores;

            this.w1Chance = stata.w1;
            this.w2Chance = stata.w2;
            this.w0Chance = stata.w3;

            this.ww1Chance = stata.ww1;
            this.ww2Chance = stata.ww2;
            this.ww0Chance = stata.ww0;

            this.OTYChance = stata.OTY;
            this.OTNChance = stata.OTN;

            console.log(this)
            // this.stata = stata.stata;
        } else {
            this.vi.$store.commit('updateChances', {
                newChances: [],
                w1: 0,
                w2: 0,
                w3: 0
            })
        }
        if (!this.vi) {
            this.mountApp.call(this)
        } else {
            if (stata) {
                this.vi.$store.commit('updateChances', {
                    newChances: stata.totals,
                    w1: stata.w1,
                    w2: stata.w2,
                    w3: stata.w3
                })
            }

        }
    }

let alanysisStrategies = [
    {
        'data-type': 9,
        'name': 'TM'
    },
    {
        'data-type': 10,
        'name': 'TL'
    },
    {
        'data-type': 401,
        'name': 'W1'
    },
    {
        'data-type': 402,
        'name': 'W2'
    },    
    {
        'data-type': 1,
        'name': 'WW1'
    },
    {
        'data-type': 2,
        'name': 'WW0'
    },    
    {
        'data-type': 3,
        'name': 'WW2'
    },    
    {
        'data-type': 731,
        'name': 'AS'
    },    
    {
        'data-type': 759,
        'name': 'OTY'
    },
    {
        'data-type': 761,
        'name': 'OTN'
    },
    {
        'data-type': 9889999,
        'name': 'LASTSCORE'
    }
    // {
    //     'data-type': 2,
    //     'name': 'W0'
    // }
]

function watchMatch(match, matchId, sportId) {
    console.log('Трекаем матчик', match)
    this.match = match;
    let ids = getMatchId(match);
    // this.sportId = matchSportId(match)
    this.sportId = sportId;
    this.matchId = ids[0];

    this.foldedS2 = false;
    this.vi = null;
    this.obj = null;
    this.s2Opened = false;
    this.moreSetsOpened = false;
    this.wholeOpened = false;
    this.matchName = extractMatchName(match);

    this.accScore = [];
    this.chances = [];

    this.w1Chance = null;
    this.w2Chance = null;
    this.w0Chance = null;

    this.ww0Chance = null;
    this.ww2Chance = null;
    this.ww1Chance = null;

    this.OTYChance = null;
    this.OTNChance = null;

    let opps = extractMatchName(match, true);
    this.opp1 = opps[0];
    this.opp2 = opps[1];

    this.buttons = {
        'TLbuttons': [],
        'TMbuttons': [],
        'W1buttons': [],
        'W2buttons': [],
        'W0buttons': [],
        'ASbuttons': [],
        'WW1buttons': [],
        'WW2buttons': [],
        'WW0buttons': [],
        'OTYbuttons': [],
        'OTNbuttons': [],
    }

    // setTimeout(getMatchAnalysis.bind(this, this.sportId, this.opp1, this.opp2, this.scores), 1600)

    this.mountApp.call(this);

    this.scores = []
    this.factQuoter = 0

    // this.strategies = JSON.parse(JSON.stringify(strategies));
    // this.strategies = (new strategies()).st

    // this.stata = history.rows.filter(r => r.name == this.matchName, this)

    // this.stata = this.stata.map(function(g) {
    //     return splitScores(g.scores)
    // })

    this.wholeSection = null;
    this.s2Section = null;
    this.moreSetsSection = null;

    this.watchTotalScoresId = customInterval(this.watchScore.bind(this), 1450, {
        name: 'Watch total score for ' + this.matchName
    });
    this.watchSetScoresId = customInterval(this.watchScore.bind(this, 1), 1450, {
        name: 'Watch 1 set score for ' + this.matchName
    });

    this.validateMatchNodeId = customInterval(this.validateMatchNode.bind(this), 1600, {
        name: 'Validate match node' + this.matchName
    });

    this.analysisTimers = alanysisStrategies.map((str) => {
        let id = customInterval(this.placeMarkers.bind(this, str), 1500, {
            name: 'analysis: ' + str.name
        })
        return {
            id,
        }
    })

    console.log(this.analysisTimers)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[0])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[1])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[2])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[3])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[4])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[5])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[6])
    // }, 8000)

    // setTimeout(() => {
    //     this.waitForCoef.call(this, this.strategies[7])
    // }, 8000)

    // setTimeout(() => {
    //     console.log(this.strategies)
    // }, 8000)
    // setTimeout(getMatchAnalysis.bind(this, this.sportId, this.opp1, this.opp2, this.scores, this.factQuoter), 1600)

}


function deleteCustomInterval(id) {
    clearInterval(id);
    let index = timeOuts.findIndex(function(int) {
        if (id == int.id) {
            return true;
        }
    })
    // console.log("Удаляем интервал", id, timeOuts[index])
    timeOuts.splice(index, 1);
}

function customInterval(fn, delay, meta) {
    let id = setInterval(fn, delay);

    timeOuts.push({
        id,
        ...meta
    })
    return id
}


// customInterval(function() {
//     console.log('TIMEOUTS')
//     console.log(timeOuts)
// }, 45000, { delay: 45000 })

// customInterval(function() {
//     console.log("MATCHES")
//     console.log(matches)
//     if (matches.length) {
//         matches.forEach(function(m) {
//             // console.log(m.wholeSection, m.s2Section)
//             // console.log(m.wholeOpened, m.s2Opened)
//         })
//     }
// }, 15000, { delay: 15000 })


watchMatch.prototype = extendWatch;

module.exports = function(core, matchesQ, matchesToUseQ, ignoredMatchesQ, timeOutsQ, pageWalkerQ, stopScanningQ) {
    pressButton = core.pressButton;
    getMatchId = core.getMatchId;
    betNotInProg = core.betNotInProg;
    validateLeague = core.validateLeague;
    ignoreMatch = core.ignoreMatch;
    isMatchIgnored = core.isMatchIgnored;
    markAs = core.markAs;
    processMatch = core.processMatch;
    extractQuater = core.extractQuater;
    extractMatchName = core.extractMatchName;
    stopAfterBet = core.stopAfterBet

    matches = matchesQ;
    matchesToUse = matchesToUseQ;
    ignoredMatches = ignoredMatchesQ;
    timeOuts = timeOutsQ;

    pageWalker = pageWalkerQ;

    stopScanning = stopScanningQ;

    return {
        watchMatch,
        searchMatches
    }
}