var matches = [];
var matchesToUse = [];
var ignoredMatches = [];
var timeOuts = [];

module.exports = {
	matches,
	matchesToUse,
	ignoredMatches,
	timeOuts
}