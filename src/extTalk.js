function sendMatchesToExt(matches) {
    // console.log(matches)
    matches = matches.map(function (m) {
        return {matchName: m.matchName, scores: m.scores}
    })
    chrome.runtime.sendMessage({ type: 'updateMatches', matches }, function(response) {});
}

module.exports = function() {

    return {
        sendMatchesToExt
    }
}