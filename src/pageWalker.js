let sportIds = {
	4: 'Tennis'	,
	3: 'Basketball',
	158: 'eSports-Table-Tennis'
}

let pressButton;
let $;

function generaterandomlivesports() {
console.log('asd')
}  

function getCurrentLives() {
	return $('.sportMenuActive, .b-filters__item.active').get().map(function(el){return $(el).attr('data-sportid') || $('a', el).attr('data-sport-color')})
}

// function removeSportFromLive(sport_name) {
// 	let link_url = `live/${sport_name}/`;
// 	let link_selector = `a[href="${link_url}"].sportMenuActive, input[data-href="${link_url}"]:checked`;

// 	let link = $(link_selector);
// 	pressButton(link[0]);
// }
function removeSportFromLive(id) {
	let sport_selector = `a[data-sport-color="${id}"].sportMenuActive, input[data-href="${id}"]:checked, .b-filters__item.active a[data-sport-color=${id}]`;

	let link = $(sport_selector);
	pressButton(link[0], true);
}

// function addSportTolive(sport_name) {
// 	let link_url = `live/${sport_name}/`;
// 	let link_selector = `a[href="${link_url}"], input[data-href="${link_url}"]`;

// 	let link = $(link_selector);
// 	pressButton(link[0]);
// }
function addSportTolive(id) {
	let sport_selector = `a[data-sport-color="${id}"], input[data-href="${id}"], .b-filters__item a[data-sport-color=${id}]`

	let link = $(sport_selector);
	pressButton(link[0], true);
}

function randomBet(delay) {
	setTimeout(function () {
		let bet_btns = $('[data-coef]').get().filter(el => $(el).text() < 2.3);
		let rnd = Math.floor( Math.random() * bet_btns.length-1 )
		pressButton(bet_btns[rnd], true)
	}, delay)
	
}

module.exports = {
	use(dep) {
		if (dep.ajax) {
			$ = dep;
		} else {
			pressButton = dep;
		}
	},
	addSportTolive,
	removeSportFromLive,
	getCurrentLives,
	randomBet
}