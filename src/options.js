var botOptions = {
    maximumCoef: 1.9,
    minimumCoef: 1.35,
    minimumChance: 0,
    maximumChance: 75,
    growCoef: 0.3,
    showHelper: true,
    autoStart: false,
    sendToBot: false
    // showHelper: false
}

chrome.storage.local.get('options', function(res) {
    if (res.options) {
        for (k in res.options) {
            botOptions[k] = res.options[k] 
        }
    } else {
        chrome.storage.local.set({
            options: botOptions
        })
    }

})

module.exports = {
    updateOptions(newOptions) {
        for (k in newOptions) {
            botOptions[k] = newOptions[k] 
        }
    },
    options: botOptions
}