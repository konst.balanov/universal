var GitRevisionPlugin = require('git-revision-webpack-plugin')
var CopyPlugin = require('copy-webpack-plugin');

const TerserPlugin = require('terser-webpack-plugin');

const HtmlWebpackPlugin = require('html-webpack-plugin');

var gitRevisionPlugin = new GitRevisionPlugin({
    branch: true
})

// const folderName = gitRevisionPlugin.branch() == 'master' ? 'BETTER' : gitRevisionPlugin.branch();
const folderName = 'universal'

const webpack = require('webpack');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
// var WebpackBuildNotifierPlugin = require('webpack-build-notifier');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled UglifyJSPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/uglifyjs-webpack-plugin
 *
 */
let mode = 0;

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var config = {
    devtool: 'inline-source-map',
    module: {
        rules: [{
            include: [path.resolve(__dirname, 'src')],
            loader: 'babel-loader',

            options: {
                plugins: ['syntax-dynamic-import'],

                presets: [
                    [
                        '@babel/preset-env',
                        {
                            modules: false
                        }
                    ]
                ]
            },

            test: /\.js$/
        }]
    },
    stats: {
        warnings: false,
        excludeModules: ["(webpack)/buildin/global.js"]
    },
    resolve: {
        extensions: ['*', '.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
        }
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        drop_console: true,
                    },
                    output: {
                        comments: false,
                    },
                },
            }),
        ],
    }
};

// 0 - DEV, 1 -PROD


let contentScript = Object.assign({}, config, {
    name: 'content',
    entry: "./src/tennisSRC.js",
    output: {
        filename: 'tennis.js',
        path: path.resolve(__dirname, folderName)
    },
    module: {
        rules: [{
                test: /\.less$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },
            {
                test: /.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /.vue$/,
                loader: 'vue-loader'
            }

        ]
    },
    plugins: [
        // make sure to include the plugin!
        new VueLoaderPlugin()
    ],
    // mode: 'development',
    mode: mode ? 'production' : 'development'
})

let extensionScript = Object.assign({}, config, {
    name: 'extension',
    output: {
        path: path.resolve(__dirname, folderName)
    },
    stats: 'none',
    plugins: [new CopyPlugin([{
                from: 'src/ext/manifest.json',
                to: 'manifest.json',
                force: true,
                toType: 'file'
            },
            {
                from: 'src/ext/images',
                to: 'images',
                toType: 'dir'
            }
        ]),

    ],
    mode: 'development',
    // mode: 'production'
})
let backgroundScript = Object.assign({}, config, {
    name: 'background',
    entry: "./src/ext/backgroundSRC.js",
    module: {
        rules: [{
            include: [path.resolve(__dirname, 'src')],
            loader: 'babel-loader',

            options: {

                presets: [
                    [
                        '@babel/preset-env',
                        {
                            modules: false
                        }
                    ]
                ]
            },

            test: /\.js$/
        }]
    },
    output: {
        filename: 'background.js',
        path: path.resolve(__dirname, folderName)
    },
    // mode: 'development',
    mode: mode ? 'production' : 'development'
})

let popupScript = Object.assign({}, config, {
    name: 'popup',
    entry: "./src/popup/popupSRC.js",
    output: {
        filename: 'popup.js',
        path: path.resolve(__dirname, folderName + '/popup')
    },
    module: {
        rules: [{
                test: /\.less$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'less-loader'
                ]
            },
            {
                test: /.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /.vue$/,
                loader: 'vue-loader'
            }

        ]
    },
    plugins: [
        // make sure to include the plugin!
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/popup/index.html'
        })
    ],
    // mode: 'development',
    mode: mode ? 'production' : 'development'
})

module.exports = [
    contentScript,
    extensionScript,
    backgroundScript,
    popupScript
]